#include <Wire.h>
#include <SoftwareSerial.h>
SoftwareSerial BT1(4,2); // RX, TX
char mensaje1;//INICIAMOS UNA CADENA
#define I2C_ADDR 0x15
int response = -1;

void setup()
{
Wire.begin(I2C_ADDR);
  Serial.begin(57600);// INICIAMOS EL SERIAL PARA MOSTRAR LOS MENSAJES
  BT1.begin(9600);
}


void loop()
{
  
  delay(2000);
  Wire.requestFrom(I2C_ADDR,20);//REALIZAMOS UNA PETICION AL CANAL 0x15 DE 20 CARACTERES (BITS)
  Serial.println("Solicitud de dato:");//IMPRIMIMOS DE QUE HEMOS PEDIDO EL MESNAJE AL DISPOSITIVO EN EL CANAL 1

    byte barr[2];
    for(int i = 0; i<2; i++){
        byte b = Wire.read();   
        barr[i] = b;   
      } 
   response = barr[1] * 256 + barr[0]; 
   
   BT1.print("El valor recibido es: ");
   BT1.println(response);
   
   Serial.println(response);//IMPRIMIMOS EL MENSAJE
  
}


