
#include <Wire.h>
#define I2C_ADDR 0x015

void setup()
{
  Wire.begin(I2C_ADDR); // Abrimos el canal 1 (0x01) del I2C
  Wire.onRequest(peticion);// Creamos el evento que se relaizará cuando el Receptor llame a el emisor (Este Arduino)
  Serial.begin(57600);
}

void loop()
{
}

void peticion() 
{
  Serial.println("Solicitud del valor en el potenciometro");//Imprimios cuando el receptor  nos pide el mensaje
  int valor_sensor = analogRead(0);    
  Serial.println(valor_sensor, DEC);
   
  byte barr[2] = { lowByte(valor_sensor), highByte(valor_sensor)};  
  Wire.write(barr,2);
  
}





